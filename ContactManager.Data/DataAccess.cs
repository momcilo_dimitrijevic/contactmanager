﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ContactManager.Data
{
    public class DataAccess
    {
        //get Contacts table
        public static Table<Contact> GetContactTable()
        {
            ContactManagerDBDataContext dbc = new ContactManagerDBDataContext();
            return dbc.GetTable<Contact>();
        }

        //get ContactTypes table
        public static Table<ContactType> GetContactTypeTable()
        {
            ContactManagerDBDataContext dbc = new ContactManagerDBDataContext();
            return dbc.GetTable<ContactType>();
        }

        //get contact details with contact type caption, ordered by date added, as a list
        public static IQueryable<ContactAndTypeResult> GetContactAndTypeInfo(int topN)
        {
            ContactManagerDBDataContext dbc = new ContactManagerDBDataContext();

            //if topN=0, return all contacts
            if (topN != 0)
            {
                return (from c in dbc.Contacts
                        join cid in dbc.ContactTypes on c.ContactTypeID equals cid.ContactTypeID
                        orderby c.InsertDate descending
                        select new ContactAndTypeResult
                        {
                            ContactID = c.ContactID,
                            FirstName = c.FirstName,
                            LastName = c.LastName,
                            Address = c.Address,
                            InsertDate = c.InsertDate.Date,
                            ContactType = cid.Caption
                        }).Take(topN);
            }
            else
            {
                return (from c in dbc.Contacts
                        join cid in dbc.ContactTypes on c.ContactTypeID equals cid.ContactTypeID
                        orderby c.InsertDate descending
                        select new ContactAndTypeResult
                        {
                            ContactID = c.ContactID,
                            FirstName = c.FirstName,
                            LastName = c.LastName,
                            Address = c.Address,
                            InsertDate = c.InsertDate.Date,
                            ContactType = cid.Caption
                        });
            }
        }

        //insert a new entry into Contacts table
        public static void InsertContact(Contact contact)
        {

            try
            {
                Table<Contact> contacts = DataAccess.GetContactTable();

                contacts.InsertOnSubmit(contact);
                contacts.Context.SubmitChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //update an existing contact
        public static void UpdateContact(Contact contact)
        {
            ContactManagerDBDataContext dbc = new ContactManagerDBDataContext();

            var target = (from c in dbc.GetTable<Contact>()
                          where c.ContactID == contact.ContactID
                          select c).SingleOrDefault(); ;

            if (target == null)
            {
                throw new Exception("Can't find contact with matching ID");
            }
            else
            {
                try
                {
                    target.FirstName = contact.FirstName;
                    target.LastName = contact.LastName;
                    target.InsertDate = contact.InsertDate;
                    target.Address = contact.Address;
                    target.ContactTypeID = contact.ContactTypeID;

                    dbc.SubmitChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        //delete contact with specified ID
        public static void DeleteContact(int ID)
        {
            ContactManagerDBDataContext dbc = new ContactManagerDBDataContext();

            var target = (from c in dbc.GetTable<Contact>()
                          where c.ContactID == ID
                          select c).SingleOrDefault(); ;

            if (target == null)
            {
                throw new Exception("Can't find contact with matching ID");
            }
            else
            {
                try
                {
                    dbc.Contacts.DeleteOnSubmit(target);
                    dbc.SubmitChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        //export contacts to file
        public static void ExportContactsToFile(string path)
        {
            List<Contact> contacts = GetContactTable().ToList();

            using (TextWriter sw = new StreamWriter(path, false))
            {
                foreach (var c in contacts)
                {
                    sw.WriteLine(c.ToString());
                }
                sw.Close();
            }
        }

        //import contacts from file
        public static string ImportContactsFromFile(string path)
        {
            int lineNum = 1;
            string errorMessage = "Insert fail on lines: \n";
            string passedMessage = "Contacts added successfully";

            bool returnErrorMessage = false;

            string[] contactString;

            var lines = File.ReadLines(path);

            foreach (var line in lines)
            {
                contactString = line.Split(';');

                Contact c = new Contact();
                
                //check if there are exactly 5 fields
                if (contactString.Length == 6 && contactString[5]=="")
                {
                    c.FirstName = contactString[0];
                    c.LastName = contactString[1];
                    c.Address = contactString[2];
                    c.InsertDate = System.Convert.ToDateTime(contactString[3]);
                    c.ContactTypeID = System.Convert.ToInt32(contactString[4]);
                    
                    try
                    {
                        DataAccess.InsertContact(c);
                    }
                    catch (ArgumentException e)
                    {
                        //contact created from text line that didn't pass validation
                        errorMessage += e.Message + " at text line " + lineNum + "\n";
                        returnErrorMessage = true;
                    }
                    catch (Exception e)
                    {
                        //contact created from text line that didn't pass validation
                        errorMessage += e.Message + " at text line " + lineNum + "\n";
                        returnErrorMessage = true;
                    }
                    //continue to next line after handling the exceptions
                }
                else
                {  
                    errorMessage += "Invalid number of fields" + " at text line " + lineNum + "\n";
                    returnErrorMessage = true;
                }

                lineNum++;           
            }
            if (returnErrorMessage)
            {
                errorMessage += "\n These contact have not been inserted.";
            }
            else
            {
                return passedMessage;
            }
            
            return errorMessage;
        }
    }

    //return type for function GetContactAndTypeInfo
    public class ContactAndTypeResult
    {
        public int ContactID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public DateTime InsertDate { get; set; }
        public string ContactType { get; set; }
    }

 
}
