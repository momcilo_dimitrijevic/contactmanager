using System;
using System.ComponentModel.DataAnnotations;

namespace ContactManager.Data
{

    partial class Contact
    {
        partial void OnValidate(System.Data.Linq.ChangeAction action)
        {
            if (this.FirstName.Equals(""))
            {
                throw new ArgumentException("First name field is required.");
            }
            
            if (this.InsertDate == DateTime.MinValue)
            {
                throw new ArgumentException("Setting InsertDate failed.");
            }
        }

        public override string ToString()
        {
            return String.Concat(this.FirstName + ";"
                               + this.LastName + ";"
                               + this.Address + ";"
                               + this.InsertDate + ";"
                               + this.ContactTypeID + ";");
        }
    }


}
