﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ContactManager.Data;

namespace ContactManager.MainForm
{
    public partial class DeleteConfirmationForm : Form
    {
        public delegate void ContactDeletedHandler(object sender);

        public event ContactDeletedHandler ContactDeleted;

        public DeleteConfirmationForm()
        {
            InitializeComponent();
        }

        public void PopulateFieldsOnDelete(object sender, ContactAndTypeResult e)
        {
            labelID.Text = e.ContactID.ToString();
            labelFirstName.Text = e.FirstName;
            labelLastName.Text = e.LastName;
            labelAddress.Text = e.Address;
            labelContactType.Text = e.ContactType;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //delete item
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DataAccess.DeleteContact(System.Convert.ToInt32(labelID.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            ContactDeleted(this);
            MessageBox.Show("Delete succesful");
            this.Close();
        }
    }
}
