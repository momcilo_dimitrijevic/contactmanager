﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ContactManager.Data;

namespace ContactManager.MainForm
{
    public partial class InputForm : Form
    {
        private string action;
        private int contactID { get; set; }

        // add a delegate
        public delegate void ContactAddedOrChangedHandler(object sender);

        // add an event of the delegate type
        public event ContactAddedOrChangedHandler ContactAddedOrChanged;

        public InputForm(string action)
        {
            this.action = action;
            InitializeComponent();
        }

        private void InputForm_Load(object sender, EventArgs e)
        {
            button1.Text = action;

            //populate ContactTypes listbox
            comboContactTypes.DataSource = DataAccess.GetContactTypeTable();

            comboContactTypes.DisplayMember = "Caption";
            comboContactTypes.ValueMember = "ContactTypeID";

            //MessageBox.Show("Starting Index  " + comboContactTypes.SelectedIndex);
        }

        public void PopulateFieldsOnUpdate(object sender, ContactAndTypeResult e)
        {
            textAddress.Text = e.Address;
            textFirstName.Text = e.FirstName;
            textLastName.Text = e.LastName;
            comboContactTypes.SelectedIndex = comboContactTypes.FindString(e.ContactType);
            this.contactID = e.ContactID;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Contact contact = new Contact();
            contact.ContactID = this.contactID;
            contact.FirstName = textFirstName.Text;
            contact.LastName = textLastName.Text;
            contact.Address = textAddress.Text;
            contact.InsertDate = System.DateTime.Now;
            contact.ContactTypeID = System.Convert.ToInt32(comboContactTypes.SelectedValue);

            try
            {
                if (action == "Add")
                {
                    DataAccess.InsertContact(contact);
                }
                else
                {
                    DataAccess.UpdateContact(contact);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            ContactAddedOrChanged(this);
            this.Close();
            
        }
    }
}
