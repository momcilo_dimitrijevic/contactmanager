﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using ContactManager.Data;

namespace ContactManager.MainForm
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ContactManager());
            /*
            ContactManagerDBDataContext dbc = new ContactManagerDBDataContext();
            
            Contact contact = new Contact();

            IQueryable<Contact> test = from c in dbc.Contacts
                                       where (c.FirstName == "Pera")
                                       select c;
             */

        }
    }
}
