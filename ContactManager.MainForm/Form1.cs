﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ContactManager.Data;

namespace ContactManager.MainForm
{
    public partial class ContactManager : Form
    {
        public delegate void ContactChangeHandler(object sender, ContactAndTypeResult e);

        public event ContactChangeHandler ContactUpdate;
        public event ContactChangeHandler ContactDelete;

        public ContactManager()
        {
            InitializeComponent();
        }

        private void importExportToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void ContactManager_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            //get data and populate datagridview
            dataGridView1.DataSource = DataAccess.GetContactAndTypeInfo(10);

            dataGridView1.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        //update selected contact
        private void button2_Click(object sender, EventArgs e)
        {
                int selectedIndex = dataGridView1.CurrentCell.RowIndex;
            
                InputForm i = new InputForm("Update");
                //prepare existing data from the selected row for transfer to inputform i
                ContactAndTypeResult data = new ContactAndTypeResult();
                
                data.ContactID = System.Convert.ToInt32(dataGridView1.Rows[selectedIndex].Cells[0].Value);
                data.FirstName = dataGridView1.Rows[selectedIndex].Cells[1].Value.ToString();
                data.LastName = dataGridView1.Rows[selectedIndex].Cells[2].Value.ToString();
                data.Address = dataGridView1.Rows[selectedIndex].Cells[3].Value.ToString();
                data.InsertDate = System.Convert.ToDateTime(dataGridView1.Rows[selectedIndex].Cells[4].Value);
                data.ContactType = dataGridView1.Rows[selectedIndex].Cells[5].Value.ToString();
                
                //assign handler to the event
                this.ContactUpdate += new ContactChangeHandler(i.PopulateFieldsOnUpdate);
                
                i.Show();
                
                //raise event to transfer existing data from datagridview to the inputform
                ContactUpdate(this, data);
                
                //refresh default newest 15 after update/insert/delete
                i.ContactAddedOrChanged += new InputForm.ContactAddedOrChangedHandler
                    (p=>dataGridView1.DataSource = DataAccess.GetContactAndTypeInfo(10));
        }

        //add new contact
        private void button3_Click(object sender, EventArgs e)
        {
            InputForm i = new InputForm("Add");
            i.Show();

            i.ContactAddedOrChanged += new InputForm.ContactAddedOrChangedHandler
                (p => dataGridView1.DataSource = DataAccess.GetContactAndTypeInfo(10));
        }
        
        //remove selected contact
        private void button1_Click(object sender, EventArgs e)
        {
            int selectedIndex = dataGridView1.CurrentCell.RowIndex;

            DeleteConfirmationForm d = new DeleteConfirmationForm();
            //prepare existing data from the selected row for transfer to inputform i
            ContactAndTypeResult data = new ContactAndTypeResult();
            data.ContactID = Convert.ToInt32(dataGridView1.Rows[selectedIndex].Cells[0].Value);
            data.FirstName = dataGridView1.Rows[selectedIndex].Cells[1].Value.ToString();
            data.LastName = dataGridView1.Rows[selectedIndex].Cells[2].Value.ToString();
            data.Address = dataGridView1.Rows[selectedIndex].Cells[3].Value.ToString();
            data.ContactType = dataGridView1.Rows[selectedIndex].Cells[5].Value.ToString();
            //assign handler to the event
            this.ContactDelete += new ContactChangeHandler(d.PopulateFieldsOnDelete);

            d.Show();

            //raise event to transfer existing data from datagridview to the inputform
            ContactDelete(this, data);

            d.ContactDeleted += new DeleteConfirmationForm.ContactDeletedHandler
                (p => dataGridView1.DataSource = DataAccess.GetContactAndTypeInfo(10));
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = DataAccess.GetContactAndTypeInfo(0);
        }

        private void exportToTextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog file = new SaveFileDialog();

            file.FileName = "contactsBackup.txt";
            file.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            
            if (file.ShowDialog() == DialogResult.OK)
            {
                DataAccess.ExportContactsToFile(file.FileName);
            }
        }

        private void importFromTextToolStripMenuItem_Click(object sender, EventArgs e)
        {

            OpenFileDialog file = new OpenFileDialog();
            file.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";

            if (file.ShowDialog() == DialogResult.OK)
            {
                string msg = DataAccess.ImportContactsFromFile(file.FileName);
                MessageBox.Show(msg);
            }
        }
    }
}
